import React, { useEffect, useState, useRef } from "react";
import {
  Container,
  Divider,
  Dropdown,
  Grid,
  Header,
  Image,
  List,
  Menu,
  Segment,
  Button
} from 'semantic-ui-react';
import domtoimage from 'dom-to-image';
import { SliderPicker } from 'react-color';
import Carousel from "react-multi-carousel";
import { usePalette } from 'react-palette'
import "react-multi-carousel/lib/styles.css";

const imageKey = [
  "2",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "11",
  "12",
  "16",
  "17",
  "18",
  "20",
  "21",
  "24",
  "32",
  "36"
]
export default function Blender() {
  const [color, setColor] = useState('#0d0504')
  const [rgb, setRgb] = useState('');
  const [hide, setHide] = useState(false);
  const [image, setImage] = useState('2');
  const [blend, setBlend] = useState('hue');
  const blendMode = [
    // { "key": "multiply", text: "multiply", value: "multiply" },
    // { "key": "screen", text: "screen", value: "screen" },
    // { "key": "overlay", text: "overlay", value: "overlay" },
    // { "key": "darken", text: "darken", value: "darken" },
    { "key": "lighten", text: "lighten", value: "lighten" },
    //{ "key": "color-dodge", text: "color-dodge", value: "color-dodge" },
    //{ "key": "color-burn", text: "color-burn", value: "color-burn" },
    { "key": "difference", text: "difference", value: "difference" },
    //{ "key": "exclusion", text: "exclusion", value: "exclusion" },
    { "key": "hue", text: "hue", value: "hue" },
    { "key": "saturation", text: "saturation", value: "saturation" },
    { "key": "color", text: "color", value: "color" },
   // { "key": "luminosity", text: "luminosity", value: "luminosity" },
  ]
  const handleColor = (e) => {
    setColor(e.target.value)
  }
  const toggleColorPicker = () => {
    document.getElementById('color-picker').click();
  }
  const downloadImage = () => {
    const scale = 2;
    domtoimage.toPng(document.getElementById('playground-image'))
      .then(function (dataUrl) {
        var link = document.createElement('a');
        link.download = 'bosphorecolor.jpeg';
        link.href = dataUrl;
        link.click();
      });
  }
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 10
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 6
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 3
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 3
    }
  };
  const { data, loading, error } = usePalette(`images/pack/${image}.jpg`)
  
  return (
      <Container style={{ marginTop: '7em' }}>
        <Grid divided="vertically">
          <Grid.Row columns={2}>
            {hide === false && 
            <Grid.Column id="playground-image">
              <input type="color" id="color-picker" className={'color-picker'} value={color} onChange={handleColor} style={{ mixBlendMode: blend }} />
              <img src={`images/pack/output/${image}_output.jpg`} />
            </Grid.Column>
            }
            {hide === true && <Grid.Column id="playground-image">
              <img src={`images/pack/${image}.jpg`} />
            </Grid.Column>}
            <Grid.Column>
              <Button primary onClick={toggleColorPicker}>Click to change color</Button>
              <Button secondary onClick={downloadImage}>Save Image</Button>
              <Button olive onClick={() => setHide(!hide)}>Toggle {hide ? "Manipulated" : "Original"} </Button>
              <Dropdown
                placeholder='Blend Mode'
                selection
                onChange={(e, { value }) => setBlend(value)}
                options={blendMode}
              />
              <div style={{ marginTop: 10 }}>
                <SliderPicker color={color} onChange={(color) => { setColor(color.hex); console.log(color) }} />
              </div>
              <div style={{ marginTop: 10 }}>
                {Object.keys(data).map(item => (
                  <div alt={item} style={{width: 50, height: 50, marginRight: 5, backgroundColor: data[item]}} />
                ))}
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Grid.Row>
        <Carousel responsive={responsive} showDots style={{ marginTop: 10 }}>
          {imageKey.map(item => (
            <div style={{ overflow: 'hidden', maxHeight: '70%' }}>
              <img src={`images/pack/${item}.jpg`} onClick={() => setImage(item)} style={{ width: '95%', height: '20vh', objectFit: 'cover' }} />
            </div>
          ))}
        </Carousel>
        </Grid.Row>
      </Container>    
  );
}
