import React from "react";
import {
  Container,
  Divider,
  Dropdown,
  Grid,
  Header,
  Image,
  List,
  Menu,
  Segment,
  Button
} from 'semantic-ui-react';

export default function Videos() {
  return (
      <>
       <Grid>
       <Grid.Row columns={2}>
            <Grid.Column>
              <iframe src="https://www.youtube.com/embed/VIm2-ID09SI" style={{width: '100%', height: '98vh'}} frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </Grid.Column>
            <Grid.Column>
              <iframe src="https://www.youtube.com/embed/08mLQw5tiNc" style={{width: '100%', height: '98vh'}} frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </Grid.Column>
            </Grid.Row>
       </Grid>
      </>
  );
}
